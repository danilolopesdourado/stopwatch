import 'package:flutter_test/flutter_test.dart';
import 'custom_controller.dart';

void main() {
  group('CustomController', () {
    late CustomController customController;

    setUp(() {
      customController = CustomController(updateScreen: () {});
    });

    test('Initial state', () {
      expect(customController.timerState, TimerState.notStarted);
      expect(customController.laps, isEmpty);
      expect(customController.timer, Duration.zero);
      expect(customController.stopwatch.isRunning, isFalse);
    });

    test('Start', () {
      customController.start();

      expect(customController.timerState, TimerState.running);
      expect(customController.stopwatch.isRunning, isTrue);
    });

    test('Pause', () {
      customController.start();
      customController.pause();

      expect(customController.timerState, TimerState.paused);
      expect(customController.stopwatch.isRunning, isFalse);
    });

    test('Reset', () {
      customController.start();
      customController.createLap();

      customController.pause();
      customController.reset();

      expect(customController.timerState, TimerState.notStarted);
      expect(customController.laps, isEmpty);
      expect(customController.timer, Duration.zero);
      expect(customController.stopwatch.isRunning, isFalse);
    });

    test('Create Lap', () {
      customController.start();
      // customController.stopwatch.elapsed = const Duration(seconds: 30);

      customController.createLap();

      expect(customController.laps.length, 1);
      expect(customController.laps.first.number, 1);
      expect(customController.stopwatch.elapsed, isNot(Duration.zero));
      // expect(customController.laps.first.total, const Duration(seconds: 30));
      // expect(customController.laps.first.variance, const Duration(seconds: 30));
    });
  });
}
