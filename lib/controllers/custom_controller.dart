import 'dart:async';

import '../models/lap.dart';

enum TimerState {
  notStarted,
  running,
  paused,
}

/// The controller used in the `CustomView` all variables and functions are here
class CustomController {
  TimerState timerState = TimerState.notStarted;
  List<Lap> laps = [];
  Duration timer = Duration.zero;
  late Timer screenUpdater;
  Stopwatch stopwatch = Stopwatch();
  final Function updateScreen;

  CustomController({required this.updateScreen});
  update() {
    updateScreen();
  }

  void reset() {
    timerState = TimerState.notStarted;
    laps.clear();
    stopwatch.reset();
    timer = Duration.zero;
    update();
  }

  void start() {
    timerState = TimerState.running;
    stopwatch.start();
    update();
  }

  void pause() {
    timerState = TimerState.paused;
    stopwatch.stop();
    update();
  }

  void createLap() {
    int elapsedMs = stopwatch.elapsed.inMilliseconds;
    Duration total = Duration(
      hours: (((elapsedMs ~/ 1000) ~/ 60) ~/ 60),
      minutes: ((elapsedMs ~/ 1000) ~/ 60),
      seconds: (elapsedMs ~/ 1000) % 60,
      milliseconds: elapsedMs % 1000,
    );
    laps.insert(
        0,
        Lap(
          number: laps.length + 1,
          variance: (laps.isNotEmpty) ? total - laps.first.total : total,
          total: total,
        ));
    update();
  }
}
