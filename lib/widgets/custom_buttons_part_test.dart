import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../controllers/custom_controller.dart';
import '../widgets/custom_buttons_part.dart';

void main() {
  group('CustomButtonsPart', () {
    testWidgets('Widget Construction', (WidgetTester tester) async {
      // Create a CustomController instance
      final customController = CustomController(updateScreen: () {});

      // Build the CustomButtonsPart widget with the CustomController
      await tester.pumpWidget(
        MaterialApp(
          home: Material(
            child: CustomButtonsPart(controller: customController),
          ),
        ),
      );

      // Verify that the CustomButtonsPart widget is rendered
      expect(find.byType(CustomButtonsPart), findsOneWidget);
    });

    testWidgets('Button Taps', (WidgetTester tester) async {
      // Create a CustomController instance
      final customController = CustomController(updateScreen: () {});

      // Build the CustomButtonsPart widget with the CustomController
      await tester.pumpWidget(
        MaterialApp(
          home: Material(
            child: StatefulBuilder(
              builder: (context, setState) {
                return CustomButtonsPart(controller: customController);
              },
            ),
          ),
        ),
      );

      // Find the Start button and tap it
      await tester.tap(find.byIcon(Icons.play_arrow));
      await tester.pump();

      // Verify that the timer state is updated to running
      expect(customController.timerState, TimerState.running);
      expect(customController.stopwatch.elapsed, isNot(Duration.zero));

      // Verify that the createLap function is called on the controller
      customController.createLap();
      expect(customController.laps.length, 1);

      // Verify that the timer state is updated to paused
      customController.pause();
      expect(customController.timerState, TimerState.paused);

      customController.reset();
      // Verify that the timer state is updated to not started
      expect(customController.timerState, TimerState.notStarted);
      // Verify that the laps list is cleared
      expect(customController.laps.isEmpty, true);
      // Verify that the stopwatch is reset
      expect(customController.stopwatch.isRunning, false);
      expect(customController.timer, Duration.zero);
    });
  });
}
