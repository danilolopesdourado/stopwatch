import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../controllers/custom_controller.dart';
import '../models/lap.dart';
import '../widgets/custom_laps_part.dart';
import 'lap_tile.dart';
import 'last_lap_tile.dart';

void main() {
  group('CustomLapsPart', () {
    testWidgets('Widget Construction with Laps', (WidgetTester tester) async {
      // Create a CustomController instance with some laps
      final customController = CustomController(updateScreen: () {});
      customController.laps = [
        Lap(
            number: 1,
            variance: const Duration(seconds: 5),
            total: const Duration(seconds: 15)),
        Lap(
            number: 2,
            variance: const Duration(seconds: 10),
            total: const Duration(seconds: 10)),
      ];

      // Build the CustomLapsPart widget with the CustomController
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: SizedBox(
              height: 200,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  CustomLapsPart(controller: customController),
                ],
              ),
            ),
          ),
        ),
      );

      // Verify that the LastLapTile widget is rendered with the first lap
      expect(find.byType(LastLapTile), findsOneWidget);
      expect(find.text('Last Lap'), findsOneWidget);

      // Verify that the LapTile widgets are rendered for each lap (except the first)
      expect(find.byType(LapTile),
          findsNWidgets(customController.laps.length - 1));
    });

    testWidgets('Widget Construction without Laps',
        (WidgetTester tester) async {
      // Create a CustomController instance without any laps
      final customController = CustomController(updateScreen: () {});

      // Build the CustomLapsPart widget with the CustomController
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: SizedBox(
              height: 200,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  CustomLapsPart(controller: customController),
                ],
              ),
            ),
          ),
        ),
      );

      // Verify that the LastLapTile widget is not rendered
      expect(find.byType(LastLapTile), findsNothing);

      // Verify that no LapTile widgets are rendered
      expect(find.byType(LapTile), findsNothing);
    });
  });
}
