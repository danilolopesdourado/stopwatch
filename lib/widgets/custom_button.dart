import 'package:flutter/material.dart';

/// Creates a round button with an Icon that  executes `onTap` when tapped.
class CustomButton extends StatelessWidget {
  final String name;
  final Icon icon;
  final void Function() onTap;
  const CustomButton({
    required this.name,
    required this.icon,
    required this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Theme.of(context).colorScheme.primary,
        ),
        child: icon,
      ),
    );
  }
}
