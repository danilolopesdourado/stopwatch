import 'package:flutter/material.dart';

import '../controllers/custom_controller.dart';
import 'lap_tile.dart';
import 'last_lap_tile.dart';

/// Creates a section that display the laps captured during the execution of the stopwatch
class CustomLapsPart extends StatelessWidget {
  final CustomController controller;
  const CustomLapsPart({super.key, required this.controller});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                if (controller.laps.isNotEmpty)
                  LastLapTile(lap: controller.laps[0]),
                Expanded(
                  child: Scrollbar(
                    child: ListView.builder(
                      itemCount: controller.laps.length,
                      itemBuilder: (context, index) => index != 0
                          ? LapTile(
                              lap: controller.laps[index],
                            )
                          : Container(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
