import 'package:flutter/material.dart';

import '../controllers/custom_controller.dart';
import '../utils/functions.dart';

/// Creates a section that display timer of the stopwatch
class CustomTimerPart extends StatelessWidget {
  final CustomController controller;
  const CustomTimerPart({super.key, required this.controller});
  @override
  Widget build(BuildContext context) {
    int elapsedMs = controller.stopwatch.elapsed.inMilliseconds;
    controller.timer = Duration(
      hours: ((elapsedMs / (1000 * 60 * 60))).truncate(),
      minutes: ((elapsedMs / (1000 * 60)) % 60).truncate(),
      seconds: ((elapsedMs / 1000) % 60).truncate(),
      milliseconds: elapsedMs % 1000,
    );
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.timer_outlined,
                color: Theme.of(context).colorScheme.primary,
                size: 32,
              ),
              const SizedBox(
                width: 16,
              ),
              Text(
                '${Utils.formatNumber(controller.timer.inHours)}:${Utils.formatNumber(controller.timer.inMinutes.remainder(60))}:${Utils.formatNumber(controller.timer.inSeconds.remainder(60))}:${Utils.formatNumber(controller.timer.inMilliseconds.remainder(1000), isThree: true)}',
                style: const TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
