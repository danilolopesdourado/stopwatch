import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../models/lap.dart';
import '../utils/functions.dart';
import '../widgets/last_lap_tile.dart';

void main() {
  group('LastLapTile', () {
    testWidgets('Widget Construction', (WidgetTester tester) async {
      // Create a Lap instance for testing
      Lap lap = Lap(
        number: 1,
        variance:
            const Duration(hours: 1, minutes: 2, seconds: 3, milliseconds: 4),
        total:
            const Duration(hours: 2, minutes: 3, seconds: 4, milliseconds: 5),
      );

      // Build the LastLapTile widget with the provided Lap instance
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: LastLapTile(lap: lap),
          ),
        ),
      );

      // Verify that the widget is constructed with the correct content
      expect(find.text('Last Lap'), findsOneWidget);
      expect(find.text('Lap Duration: '), findsOneWidget);
      expect(find.text(lap.representateVariance()), findsOneWidget);
      expect(find.text('# Laps: '), findsOneWidget);
      expect(find.text(Utils.formatNumber(lap.number)), findsOneWidget);
      expect(find.text('Total Time: '), findsOneWidget);
      expect(find.text(lap.representateTotal()), findsOneWidget);
    });
  });
}
