import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../controllers/custom_controller.dart';
import '../widgets/custom_timer_part.dart';

void main() {
  group('CustomTimerPart', () {
    testWidgets('Widget Construction', (WidgetTester tester) async {
      // Create a CustomController instance with a running stopwatch
      final customController = CustomController(updateScreen: () {});
      // Build the CustomTimerPart widget with the CustomController
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CustomTimerPart(controller: customController),
          ),
        ),
      );

      // Verify that the timer text is correctly displayed
      expect(find.text('00:00:00:000'), findsOneWidget);
      customController.timerState = TimerState.running;

      // Verify that the timer text is updated
      expect(customController.timerState, TimerState.running);
    });
  });
}
