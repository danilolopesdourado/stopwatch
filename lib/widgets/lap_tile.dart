import 'package:flutter/material.dart';

import '../models/lap.dart';
import '../utils/functions.dart';

/// The widget used to show older laps
class LapTile extends StatelessWidget {
  final Lap lap;
  const LapTile({required this.lap, super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.flag_outlined,
              color: Theme.of(context).colorScheme.primary,
            ),
            Text(Utils.formatNumber(lap.number)),
          ],
        ),
        Text(lap.representateVariance()),
        Text(lap.representateTotal()),
      ],
    );
  }
}
