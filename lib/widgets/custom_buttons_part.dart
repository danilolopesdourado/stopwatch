import 'package:flutter/material.dart';

import '../controllers/custom_controller.dart';
import 'custom_button.dart';

/// Creates a section where the buttons are displayed depending on the state of the timer
class CustomButtonsPart extends StatelessWidget {
  final CustomController controller;
  const CustomButtonsPart({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              controller.timerState == TimerState.paused
                  ? CustomButton(
                      name: 'Reset',
                      icon: Icon(
                        Icons.restore,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      onTap: () => controller.reset(),
                    )
                  : const SizedBox(
                      height: 56,
                      width: 56,
                    ),
              controller.timerState == TimerState.running
                  ? CustomButton(
                      name: 'Pause',
                      icon: Icon(
                        Icons.pause,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      onTap: () => controller.pause(),
                    )
                  : CustomButton(
                      name: 'Start',
                      icon: Icon(
                        Icons.play_arrow,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      onTap: () => controller.start(),
                    ),
              controller.timerState == TimerState.running
                  ? CustomButton(
                      name: 'Lap',
                      icon: Icon(
                        Icons.flag,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                      onTap: () => controller.createLap(),
                    )
                  : const SizedBox(
                      height: 56,
                      width: 56,
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
