import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import '../widgets/custom_button.dart';

void main() {
  group('CustomButton', () {
    testWidgets('Widget Construction', (WidgetTester tester) async {
      // Create variables for the button's properties
      String name = 'Button';
      Icon icon = const Icon(Icons.add);
      bool tapped = false;

      // Build the CustomButton widget
      await tester.pumpWidget(
        MaterialApp(
          home: Material(
            child: CustomButton(
              name: name,
              icon: icon,
              onTap: () {
                tapped = true;
              },
            ),
          ),
        ),
      );

      // Find the CustomButton widget
      final customButtonFinder = find.byType(CustomButton);

      // Verify that the CustomButton widget is rendered
      expect(customButtonFinder, findsOneWidget);

      // Verify that the CustomButton widget contains the specified icon
      expect(find.byIcon(Icons.add), findsOneWidget);

      // Tap the CustomButton widget
      await tester.tap(customButtonFinder);

      // Rebuild the widget after the tap
      await tester.pump();

      // Verify that the onTap function was executed
      expect(tapped, true);
    });
  });
}
