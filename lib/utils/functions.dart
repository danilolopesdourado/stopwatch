/// Class with static methods used in the application
class Utils {
  static String formatNumber(int number, {bool isThree = false}) {
    return number
        .toString()
        .padLeft(isThree ? 3 : 2, '0'); //.padRight(isThree ? 3 : 2, '0');
  }
}
