import 'package:flutter_test/flutter_test.dart';
import '../utils/functions.dart';

void main() {
  group('Utils', () {
    test('FormatNumber - Two digits', () {
      // Test formatNumber with a two-digit number
      int number = 5;
      String formattedNumber = Utils.formatNumber(number);

      expect(formattedNumber, equals('05'));
    });

    test('FormatNumber - Three digits', () {
      // Test formatNumber with a three-digit number
      int number = 123;
      String formattedNumber = Utils.formatNumber(number, isThree: true);

      expect(formattedNumber, equals('123'));
    });

    test('FormatNumber - Three digits with leading zero', () {
      // Test formatNumber with a three-digit number and leading zero
      int number = 5;
      String formattedNumber = Utils.formatNumber(number, isThree: true);

      expect(formattedNumber, equals('005'));
    });
  });
}
