import '../utils/functions.dart';

/// Model that represents the laps
class Lap {
  final int number;
  final Duration variance;
  final Duration total;

  Lap({
    required this.number,
    required this.variance,
    required this.total,
  });

  String representateVariance() {
    return '+ ${Utils.formatNumber(variance.inHours)}:${Utils.formatNumber(variance.inMinutes.remainder(60))}:${Utils.formatNumber(variance.inSeconds.remainder(60))}:${Utils.formatNumber(variance.inMilliseconds.remainder(1000), isThree: true)}';
  }

  String representateTotal() {
    return '${Utils.formatNumber(total.inHours)}:${Utils.formatNumber(total.inMinutes.remainder(60))}:${Utils.formatNumber(total.inSeconds.remainder(60))}:${Utils.formatNumber(total.inMilliseconds.remainder(1000), isThree: true)}';
  }
}
