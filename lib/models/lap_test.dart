import 'package:flutter_test/flutter_test.dart';
import '../models/lap.dart';

void main() {
  group('Lap', () {
    test('Representate Variance', () {
      // Create a Lap instance with specific variance
      Lap lap = Lap(
        number: 1,
        variance:
            const Duration(hours: 1, minutes: 2, seconds: 3, milliseconds: 4),
        total: Duration.zero,
      );

      // Expected representation of variance
      String expectedRepresentation = '+ 01:02:03:004';

      // Assert that the representation of variance matches the expected string
      expect(lap.representateVariance(), expectedRepresentation);
    });

    test('Representate Total', () {
      // Create a Lap instance with specific total duration
      Lap lap = Lap(
        number: 1,
        variance: Duration.zero,
        total:
            const Duration(hours: 1, minutes: 2, seconds: 3, milliseconds: 4),
      );

      // Expected representation of total duration
      String expectedRepresentation = '01:02:03:004';

      // Assert that the representation of total duration matches the expected string
      expect(lap.representateTotal(), expectedRepresentation);
    });
  });
}
