import 'package:flutter_test/flutter_test.dart';
import '../main.dart';
import '../widgets/custom_buttons_part.dart';
import '../widgets/custom_laps_part.dart';
import '../widgets/custom_timer_part.dart';

void main() {
  group('CustomView', () {
    testWidgets('Widget Construction', (WidgetTester tester) async {
      // Build the CustomView widget
      await tester.pumpWidget(
        const MyApp(),
      );

      // Verify that the CustomTimerPart, CustomLapsPart, and CustomButtonsPart widgets are rendered
      expect(find.byType(CustomTimerPart), findsOneWidget);
      expect(find.byType(CustomLapsPart), findsOneWidget);
      expect(find.byType(CustomButtonsPart), findsOneWidget);
    });
  });
}
