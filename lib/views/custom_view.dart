import 'dart:async';

import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../controllers/custom_controller.dart';
import '../widgets/custom_buttons_part.dart';
import '../widgets/custom_laps_part.dart';
import '../widgets/custom_timer_part.dart';

/// Main screen of the application, this is the root where other widgets
/// and the controller logic is applied
class CustomView extends StatefulWidget {
  const CustomView({super.key});

  @override
  State<CustomView> createState() => _CustomViewState();
}

class _CustomViewState extends State<CustomView> {
  late CustomController controller;

  @override
  void initState() {
    controller = CustomController(updateScreen: () => setState(() {}));
    super.initState();
    controller.screenUpdater = Timer.periodic(
      const Duration(milliseconds: 40),
      (timer) {
        if (controller.timerState == TimerState.running) {
          controller.update();
        }
      },
    );
  }

  @override
  void dispose() {
    controller.screenUpdater.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.background,
        title: Text(
          'Stopwatch APP',
          style: TextStyle(color: Theme.of(context).colorScheme.primary),
        ),
        centerTitle: true,
      ),
      body: ResponsiveBreakpoints.of(context).largerThan(TABLET)
          ? Center(
              child: Container(
                constraints:
                    const BoxConstraints(maxWidth: 800, maxHeight: 600),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: CustomTimerPart(controller: controller),
                          ),
                          Expanded(
                            child: CustomButtonsPart(controller: controller),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          CustomLapsPart(controller: controller),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          : Column(
              children: [
                CustomTimerPart(controller: controller),
                CustomLapsPart(controller: controller),
                CustomButtonsPart(controller: controller),
              ],
            ),
    );
  }
}
