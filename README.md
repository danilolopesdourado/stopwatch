# Stopwatch

You work for a company that makes stopwatches. Oftentimes, though, a given stopwatch evolves into something much larger with many additional features beyond what would be considered part of a standard stopwatch.

The company would like to be able to deploy to many environments using a single codebase if possible.

You have been newly hired and have been asked to build out a production ready, functional and deployable interface for the base case. Here are the requirements:

- As a user I want to be able to start the stopwatch so that I can begin tracking time
- As a user I want to be able to stop the stopwatch so that I can end tracking time
- As a user I want to be able to optionally track laps
- When tracking laps, I want to know what order they were tracked in
- When tracking laps, I want to know how many there are
- As a user I want to be able to reset the stopwatch
- Resetting means that all laps are reset
- Resetting means that overall time is also reset

The finished product should, at a minimum, build to iOS and Android devices (Simulator/Emulator is okay) and to web displaying a uniform interface and user experience. The finished product should include everything needed for other engineers to work on the product and for the deployment team to deploy the application (into the App or Play stores or to web). The finished product should provide confidence that the application will perform as expected.

## Using the APP

To use the app is pretty straightforward:
- Tap the start button to start the timer
- Press the lap button while the timer is running to create Laps
- Tap the pause button while the timer is running to stop the timer
- Tap the reset button while the timer is stopped and is not zero to reset the laps and the timer

## Prototype

![Stopwatch Prototype](gitfiles/prototype.png?raw=true)

## Code Coverage

The unit tests written for this app covers over 95% of the total code.

![Code Coverage](gitfiles/coverage.png?raw=true)

## Installation

Follow the default steps for installing flutter in your machine, every step matters, so make sure your `flutter doctor` command have `No issues found!`.

[Flutter Installation](https://docs.flutter.dev/get-started/install/windows/mobile?tab=download)

For this challenge, the flutter version used was **Flutter 3.19.3**.

You can run the application with `flutter run` or pressing the Run/Debug button in your IDE while you have a emulator/simulator running or connected to a physical device through USB cable or ADB.

If you are having trouble installing because you have a different version of flutter in your machine try FVM.

[FVM Installation](https://fvm.app/)

## Additional Information

In this project, the default theme (dark, light) is the default of the machine/emulator, to change the app theme, simply change the host theme temporarily.

## GIFs

![Stopwatch Prototype GIF](gitfiles/screenrecord.gif?raw=true)

![Stopwatch Prototype Responsive](gitfiles/screenrecordresponsive.gif?raw=true)

## Screenshots

![Stopwatch Prototype NotStarted](gitfiles/notstarted.png?raw=true)
![Stopwatch Prototype Running](gitfiles/running.png?raw=true)
![Stopwatch Prototype Laps](gitfiles/laps.png?raw=true)
![Stopwatch Prototype Paused](gitfiles/paused.png?raw=true)

