import 'package:stopwatch/controllers/custom_controller_test.dart'
    as custom_controller_tests;
import 'package:stopwatch/models/lap_test.dart' as lap_tests;
import 'package:stopwatch/utils/functions_test.dart' as functions_tests;
import 'package:stopwatch/widgets/lap_tile_test.dart' as lap_tile_tests;
import 'package:stopwatch/widgets/last_lap_tile_test.dart'
    as last_lap_tile_tests;
import 'package:stopwatch/widgets/custom_button_test.dart'
    as custom_button_tests;
import 'package:stopwatch/widgets/custom_buttons_part_test.dart'
    as custom_buttons_part_tests;
import 'package:stopwatch/widgets/custom_laps_part_test.dart'
    as custom_laps_part_tests;
import 'package:stopwatch/widgets/custom_timer_part_test.dart'
    as custom_timer_part_tests;
import 'package:stopwatch/views/custom_view_test.dart' as custom_view_tests;

void main() {
  custom_controller_tests.main();
  lap_tests.main();
  functions_tests.main();
  lap_tile_tests.main();
  last_lap_tile_tests.main();
  custom_button_tests.main();
  custom_buttons_part_tests.main();
  custom_laps_part_tests.main();
  custom_timer_part_tests.main();
  custom_view_tests.main();
}
